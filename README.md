Contents
--------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * See Also


Introduction
------------

The Postoffice module sends themed emails with
[Symfony Mailer](https://symfony.com/doc/current/mailer.html). This module
provides a small, stable and clean API for developers to work with.

 * For details on how to use the API in a project, take a look at the
   [guide](https://www.drupal.org/docs/contributed-modules/postoffice)
   and the
   [examples](https://git.drupalcode.org/project/postoffice/-/tree/1.0.x/examples)
 * For a full description of the module, visit the
   [project page](https://www.drupal.org/project/postoffice)
 * To submit bug reports and feature suggestions, or track changes, visit the
   [issue tracker](https://www.drupal.org/project/issues/postoffice)


Requirements
------------

This module requires [symfony/mailer](https://packagist.org/packages/symfony/mailer)
5 (for Drupal 9) or 6 (for Drupal 10). Optional extensions require additional
composer libraries:

 * Postoffice Html2Text Styles requires
   [soundasleep/html2text](https://packagist.org/packages/soundasleep/html2text).
 * Postoffice Inline Styles requires
   [tijsverkoyen/css-to-inline-styles](https://packagist.org/packages/tijsverkoyen/css-to-inline-styles)


Installation
------------

 * Install as you would normally install a contributed Drupal module. Visit the
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
   documentation page for further information.


Configuration
-------------

 * Change symfony mailer transport DSN and mail theme in
   *Administration » Configuration » System » Postoffice settings*
 * (Optional) Enable *Postoffice Inline Styles* module in order to inline
   styles from libraries referenced by templates into the generated markup.
 * (Optional) Enable *Postoffice Html2Text* module in order to generate a plain
   text part automatically.
 * (Optional) Enable *Postoffice Skel* module in order to wrap all HTML mails
   into a HTML document.
 * (Optional) Enable *Postoffice Compat* module and configure one or more of the
   included compatibility mail plugins:
   * (Optional) Send mails from the core user module using Postoffice:
     ```
     drush config:set system.mail interface.user postoffice_user_mail
     ```
   * (Optional) Send mails from the core contact module using Postoffice:
     ```
     drush config:set system.mail interface.contact postoffice_contact_mail
     ```
  * (Optional) Enable *Postoffice Compat Theme* module to ensure that all mail
    handled by core mail manager is rendered using the theme configured for
    Postoffice.
  * (Optional) Enable *Postoffice Twig* and use additional filters and functions
    in email twig templates:
    * `postoffice_subject`: Allows Twig templates to set a subject (usable as
      function and filter).
    * `postoffice_text_body`: Allows Twig templates to attach a text body part.
  * (Optional) Enable *Postoffice Image* and use additional filters and
    functions in email twig templates:
    * `postoffice_image_embed`: Embeds an image specified by its uri and
      optionally applies the given image style.
  * (Optional) Enable *Postoffice File* and use additional functions in email
    twig templates:
    * `postoffice_file_attach_entity`: Attaches a managed file entity.
    * `postoffice_file_attach_uri`: Attaches a file specified by its uri.

See Also
--------

More contrib integration modules are developed in separate projects:

 * [Postoffice Commerce](https://www.drupal.org/project/postoffice_commerce)
 * [Postoffice Simplenews](https://www.drupal.org/project/postoffice_simplenews)


This module is geared towards developers. If it does not suit your modus
operandi, please have a look at the
[Symfony Mailer](https://www.drupal.org/project/symfony_mailer) module.


Maintainers
-----------

Current maintainers:

 * [znerol](https://www.drupal.org/u/znerol)
