<?php

namespace Drupal\postoffice_form_example\Email;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Drupal\postoffice\Email\TemplateAttachmentsTrait;
use Drupal\postoffice\Email\ThemedEmailInterface;
use Symfony\Component\Mime\Email as SymfonyEmail;

/**
 * Example email.
 */
class TestEmail extends SymfonyEmail implements ThemedEmailInterface, TemplateAttachmentsInterface {

  use StringTranslationTrait;
  use TemplateAttachmentsTrait;

  /**
   * The person to greet.
   */
  protected string $name;

  /**
   * The system.site config.
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a new test mail.
   */
  public function __construct(string $name, ?ImmutableConfig $config = NULL) {
    parent::__construct();
    $this->name = $name;
    $this->config = $config ?? \Drupal::config('system.site');
  }

  /**
   * {@inheritdoc}
   */
  public function buildThemedEmail(): ?array {
    $this->subject($this->t('Greetings from @site-name', [
      '@site-name' => $this->config->get('name'),
    ]));

    return [
      '#theme' => 'email_greetings',
      '#name' => $this->name,
      '#subject' => $this->getSubject(),
    ];
  }

}
