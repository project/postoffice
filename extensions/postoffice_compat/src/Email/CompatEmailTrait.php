<?php

namespace Drupal\postoffice_compat\Email;

/**
 * Provides utility methods for compat email implementations.
 */
trait CompatEmailTrait {

  /**
   * Populate headers from the given message.
   */
  public function headersFromMessage(array $message): static {
    $headers = $this->getHeaders();
    foreach ($message['headers'] as $name => $value) {
      if (!$this->isIgnorableHeader($name)) {
        if ($this->isMailboxListHeader($name)) {
          // Split values by comma, but ignore commas encapsulated in double
          // quotes.
          $value = str_getcsv($value, ',');
        }
        $headers->addHeader($name, $value);
      }
    }
    return $this;
  }

  /**
   * Return TRUE if a given header can contain multiple email addresses.
   *
   * @see \Symfony\Component\Mime\Header\Headers::HEADER_CLASS_MAP
   */
  protected function isMailboxListHeader(string $name): bool {
    return in_array(
      strtolower($name), ['from', 'to', 'reply-to', 'cc', 'bcc'],
      TRUE
    );
  }

  /**
   * Return TRUE if a given header should be ignored.
   *
   * Those headers are taken care of by symfony mailer.
   */
  protected function isIgnorableHeader(string $name): bool {
    return in_array(strtolower($name), [
      'mime-version',
      'content-type',
      'content-transfer-encoding',
      'x-mailer',
      'return-path',
    ], TRUE);
  }

}
