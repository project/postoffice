<?php

namespace Drupal\postoffice_compat\Email;

use Drupal\contact\ContactFormInterface;

/**
 * Interface for contact email originating from a contact form.
 */
interface ContactFormEmailInterface {

  /**
   * Returns the contact form object.
   *
   * Exposes, e.g., email.contactForm.reply to twig templates.
   */
  public function getContactForm(): ContactFormInterface;

  /**
   * Returns contact form label.
   *
   * Accessible via email.contactFormLabel from twig templates.
   */
  public function getContactFormLabel(): string;

  /**
   * Returns contact form url.
   *
   * Accessible via email.contactFormUrl from twig templates.
   */
  public function getContactFormUrl(): string;

}
