<?php

namespace Drupal\postoffice_compat\Email;

use Drupal\contact\ContactFormInterface;

/**
 * Implements the ContactFormEmailInterface.
 */
trait ContactFormEmailTrait {

  /**
   * The contact form.
   */
  protected ContactFormInterface $contactForm;

  /**
   * {@inheritdoc}
   */
  public function getContactForm(): ContactFormInterface {
    return $this->contactForm;
  }

  /**
   * {@inheritdoc}
   */
  public function getContactFormLabel(): string {
    return $this->contactForm->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getContactFormUrl(): string {
    return $this->contactForm->toUrl('canonical', $this->getUrlOptions())->toString();
  }

}
