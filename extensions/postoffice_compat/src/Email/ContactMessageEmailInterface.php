<?php

namespace Drupal\postoffice_compat\Email;

use Drupal\contact\MessageInterface;

/**
 * Interface for contact email containing a contact message.
 */
interface ContactMessageEmailInterface {

  /**
   * Returns contact message object.
   *
   * Accessible via email.contactMessage from twig templates.
   *
   * @see \Drupal\contact\MessageInterface
   */
  public function getContactMessage(): MessageInterface;

  /**
   * Returns TRUE if this is a copy going to the sender.
   *
   * Accessible via email.copy from twig templates.
   */
  public function isCopy(): bool;

  /**
   * Returns the sender name.
   *
   * Accessible via email.senderName from twig templates.
   */
  public function getSenderName(): string;

  /**
   * Returns the sender email.
   *
   * Accessible via email.senderEmail from twig templates.
   */
  public function getSenderEmail(): string;

  /**
   * Returns sender canonical url if sent from an existing account.
   *
   * Accessible via email.senderCanonicalUrl from twig templates.
   */
  public function getSenderCanonicalUrl(): ?string;

}
