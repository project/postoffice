<?php

namespace Drupal\postoffice_compat\Email;

use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\contact\MessageInterface;
use Drupal\user\UserInterface;

/**
 * Implements the ContactMessageEmailInterface.
 */
trait ContactMessageEmailTrait {

  /**
   * The contact message.
   */
  protected MessageInterface $contactMessage;

  /**
   * The sending user (logged in or anonymous user).
   */
  protected UserInterface $contactSender;

  /**
   * The contact message view builder.
   */
  protected EntityViewBuilderInterface $contactMessageViewBuilder;

  /**
   * Whether or not this message is a copy for the sender.
   */
  protected bool $contactCopy;

  /**
   * {@inheritdoc}
   */
  public function getContactMessage(): MessageInterface {
    return $this->contactMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function isCopy(): bool {
    return $this->contactCopy;
  }

  /**
   * {@inheritdoc}
   */
  public function getSenderName(): string {
    return $this->contactMessage->getSenderName();
  }

  /**
   * {@inheritdoc}
   */
  public function getSenderEmail(): string {
    return $this->contactMessage->getSenderMail();
  }

  /**
   * {@inheritdoc}
   */
  public function getSenderCanonicalUrl(): ?string {
    return $this->contactSender->isAuthenticated() ?
      $this->contactSender->toUrl('canonical', $this->getUrlOptions())->toString() :
      NULL;
  }

  /**
   * Renders the contact message.
   */
  protected function viewContactMessage(): array {
    return $this->getContactMessageViewBuilder()
      ->view($this->contactMessage, 'mail', $this->getLangcode());
  }

  /**
   * Returns the contact message view builder.
   */
  protected function getContactMessageViewBuilder(): EntityViewBuilderInterface {
    return ($this->contactMessageViewBuilder ?? \Drupal::entityTypeManager())
      ->getViewBuilder('contact_message');
  }

}
