<?php

namespace Drupal\postoffice_compat\Email;

/**
 * Interface for contact email containing a personal contact message.
 */
interface ContactPersonalEmailInterface {

  /**
   * Returns recipient name if this is a message to a personal form.
   *
   * Accessible via email.recipientName from twig templates.
   */
  public function getRecipientName(): string;

  /**
   * Returns recipient edit url if this is a message to a personal form.
   *
   * Accessible via email.recipientEditUrl from twig templates.
   */
  public function getRecipientEditUrl(): string;

}
