<?php

namespace Drupal\postoffice_compat\Email;

use Drupal\user\UserInterface;

/**
 * Interface for user email.
 */
interface UserEmailInterface {

  /**
   * Returns the account cancel url.
   *
   * Accessible via email.accountCancelUrl from twig templates.
   */
  public function getAccountCancelUrl(): string;

  /**
   * Returns a one time login link.
   *
   * Accessible via email.accountOneTimeLoginUrl from twig templates.
   */
  public function getAccountOneTimeLoginUrl(): string;

  /**
   * Returns the canonical url to the account.
   *
   * Accessible via email.accountCanonicalUrl from twig templates.
   */
  public function getAccountCanonicalUrl(): string;

  /**
   * Returns the edit url for the account.
   *
   * Accessible via email.accountEditUrl from twig templates.
   */
  public function getAccountEditUrl(): string;

  /**
   * Returns the account object.
   *
   * Accessible via email.account.displayName, email.account.name,
   * email.account.email etc. in twig templates.
   */
  public function getAccount(): UserInterface;

}
