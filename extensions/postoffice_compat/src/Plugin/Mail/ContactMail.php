<?php

namespace Drupal\postoffice_compat\Plugin\Mail;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\postoffice_compat\Email\ContactAutoreplyEmail;
use Drupal\postoffice_compat\Email\ContactPageEmail;
use Drupal\postoffice_compat\Email\ContactPersonalEmail;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Mail backend plugin for contact module using Symfony Mailer via Postoffice.
 *
 * @Mail(
 *   id = "postoffice_contact_mail",
 *   label = @Translation("Postoffice Contact Mail"),
 *   description = @Translation("Sends message from the core contact module, using Symfony Mailer via Postoffice.")
 * )
 */
class ContactMail extends CompatMailBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('postoffice.mailer'),
      $container->get('logger.channel.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    // Remove body. Contact module puts together a mix of HTML and plain text.
    // Thus it is easier to simply recreate the message from within the
    // template.
    $message['body'] = '';
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  protected function emailFromMessage(array $message): RawMessage {
    $isCopy = FALSE;
    switch ($message['id']) {
      case 'contact_page_autoreply':
        return ContactAutoreplyEmail::createFromMessage($message);

      case 'contact_page_copy':
        $isCopy = TRUE;
        // fallthrough.
      case 'contact_page_mail':
        return ContactPageEmail::createFromMessage($message, $isCopy);

      case 'contact_user_copy':
        $isCopy = TRUE;
        // fallthrough.
      case 'contact_user_mail':
        return ContactPersonalEmail::createFromMessage($message, $isCopy);

      default:
        throw new \InvalidArgumentException(static::class . ' cannot handle message with id ' . $message['id']);
    }
  }

}
