<?php

namespace Drupal\postoffice_compat\Plugin\Mail;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\postoffice_compat\Email\FallbackEmail;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Defines a fallback mail backend using Symfony Mailer via Postoffice.
 *
 * @Mail(
 *   id = "postoffice_fallback_mail",
 *   label = @Translation("Postoffice Fallback Mail"),
 *   description = @Translation("Sends a message using Symfony Mailer via Postoffice.")
 * )
 */
class FallbackMail extends CompatMailBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('postoffice.mailer'),
      $container->get('logger.channel.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function emailFromMessage(array $message): RawMessage {
    return FallbackEmail::createFromMessage($message);
  }

}
