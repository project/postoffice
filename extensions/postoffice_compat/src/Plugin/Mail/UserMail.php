<?php

namespace Drupal\postoffice_compat\Plugin\Mail;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\postoffice_compat\Email\UserEmail;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Defines a mail backend for user module, using Symfony Mailer via Postoffice.
 *
 * @Mail(
 *   id = "postoffice_user_mail",
 *   label = @Translation("Postoffice User Mail"),
 *   description = @Translation("Sends message from the core user module, using Symfony Mailer via Postoffice.")
 * )
 */
class UserMail extends CompatMailBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('postoffice.mailer'),
      $container->get('logger.channel.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function emailFromMessage(array $message): RawMessage {
    return UserEmail::createFromMessage($message);
  }

}
