<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;

/**
 * Base class for postoffice compat kernel tests.
 *
 * @group postoffice_compat
 */
abstract class CompatTestBase extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['filter', 'postoffice_compat'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['filter', 'postoffice_compat']);
  }

  /**
   * Constructs a core message array.
   *
   * @see \Drupal\Core\Mail\MailManager::doMail()
   */
  protected function createCoreMessage(string $module, string $key, string $to = 'simpletest@example.com', string $langcode = 'en', array $params = [], string $reply = 'simpletest@example.com') {
    $siteMail = 'simpletest@example.com';
    $headers = [
      'MIME-Version' => '1.0',
      'Content-Type' => 'text/plain; charset=UTF-8; format=flowed; delsp=yes',
      'Content-Transfer-Encoding' => '8Bit',
      'X-Mailer' => 'Drupal',
    ];
    $headers['From'] = $headers['Sender'] = $headers['Return-Path'] = $siteMail;
    if ($reply) {
      $headers['Reply-to'] = $reply;
    }

    return [
      'id' => $module . '_' . $key,
      'module' => $module,
      'key' => $key,
      'to' => $to,
      'from' => 'simpletest@example.com',
      'reply-to' => $reply,
      'langcode' => $langcode,
      'params' => $params,
      'send' => TRUE,
      'subject' => '',
      'body' => '',
      'headers' => $headers,
    ];
  }

}
