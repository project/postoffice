<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\contact\Entity\ContactForm;
use Drupal\postoffice_compat\Email\ContactAutoreplyEmail;

/**
 * Tests for contact autoreply email.
 *
 * @group postoffice_compat
 */
class ContactAutoreplyEmailTest extends CompatTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['contact', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['contact']);
    $this->installEntitySchema('contact_message');
  }

  /**
   * Verify that contact form properties and urls are accessible via twig.
   */
  public function testContactAutoreplyTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_compat_test_theme')->save();

    // Tests email.contactFormLabel
    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'contact_form_label_test',
      'label' => $this->randomMachineName(),
    ]);
    $contactForm->save();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $email = ContactAutoreplyEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertEquals($contactForm->label(), $actual);

    // Tests email.contactFormUrl
    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'contact_form_url_test',
      'label' => $this->randomMachineName(),
    ]);
    $contactForm->save();
    $coreMessage = $this->createCoreMessage('user', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $email = ContactAutoreplyEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $expected = $contactForm->toUrl('canonical', ['absolute' => TRUE])->toString();
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that the autoreply body is accessible via twig.
   */
  public function testContactAutoreplyBodyVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_compat_test_theme')->save();

    $replyLines = [
      $this->randomString(32),
      $this->randomString(28),
      $this->randomString(8),
    ];

    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'body_test',
      'label' => $this->randomMachineName(),
      'reply' => implode("\n\n", $replyLines),
    ]);
    $contactForm->save();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $email = ContactAutoreplyEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $expected = trim(implode("\n", array_map(
      fn($line) => '<p>' . Html::escape($line) . '</p>',
      $replyLines,
    )));
    $this->assertEquals($expected, $actual);
  }

}
