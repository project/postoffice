<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\contact\Entity\ContactForm;
use Drupal\contact\Entity\Message;
use Drupal\postoffice_compat\Email\ContactPageEmail;
use Drupal\user\Entity\User;

// cspell:ignore mesg

/**
 * Tests for page contact email.
 *
 * @group postoffice_compat
 */
class ContactPageEmailTest extends CompatTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['contact', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['contact', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('contact_message');
    $this->installEntitySchema('user');
  }

  /**
   * Verify that email.copy property is accessible via twig.
   */
  public function testContactPageCopyTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_test_theme')->save();

    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'contact_mesg_copy_test',
      'label' => $this->randomMachineName(),
    ]);
    $contactForm->save();

    $contactMessage = $message = Message::create([
      'contact_form' => 'contact_mesg_copy_test',
      'subject' => $this->randomMachineName(),
      'message' => $this->randomString(),
      'mail' => 'simpletest@example.com',
      'name' => $this->randomString(),
      'copy' => TRUE,
    ]);
    $message->save();

    $sender = User::getAnonymousUser();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPageEmail::createFromMessage($coreMessage, FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertEquals('orig', $actual);

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPageEmail::createFromMessage($coreMessage, TRUE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertEquals('copy', $actual);
  }

  /**
   * Verify that email.senderName property is accessible via twig.
   */
  public function testContactPageSenderNameTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_test_theme')->save();

    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'contact_mesg_sender_name_test',
      'label' => $this->randomMachineName(),
    ]);
    $contactForm->save();

    $contactMessage = $message = Message::create([
      'contact_form' => 'contact_mesg_sender_name_test',
      'subject' => $this->randomMachineName(),
      'message' => $this->randomString(),
      'mail' => 'simpletest@example.com',
      'name' => $this->randomString(),
      'copy' => TRUE,
    ]);
    $message->save();

    $sender = User::getAnonymousUser();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPageEmail::createFromMessage($coreMessage, FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $expected = Html::escape($contactMessage->getSenderName());
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that email.senderEmail property is accessible via twig.
   */
  public function testContactPageSenderEmailTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_test_theme')->save();

    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'contact_mesg_sender_email_test',
      'label' => $this->randomMachineName(),
    ]);
    $contactForm->save();

    $contactMessage = $message = Message::create([
      'contact_form' => 'contact_mesg_sender_email_test',
      'subject' => $this->randomMachineName(),
      'message' => $this->randomString(),
      'mail' => $this->randomMachineName() . '@example.com',
      'name' => $this->randomString(),
      'copy' => TRUE,
    ]);
    $message->save();

    $sender = User::getAnonymousUser();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPageEmail::createFromMessage($coreMessage, FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertEquals($contactMessage->getSenderMail(), $actual);
  }

  /**
   * Verify that email.senderCanonicalUrl property is accessible via twig.
   */
  public function testContactPageSenderUrlTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_test_theme')->save();

    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'contact_mesg_sender_url_test',
      'label' => $this->randomMachineName(),
    ]);
    $contactForm->save();

    $contactMessage = $message = Message::create([
      'contact_form' => 'contact_mesg_sender_url_test',
      'subject' => $this->randomMachineName(),
      'message' => $this->randomString(),
      'mail' => $this->randomMachineName() . '@example.com',
      'name' => $this->randomString(),
      'copy' => TRUE,
    ]);
    $message->save();

    $sender = User::getAnonymousUser();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPageEmail::createFromMessage($coreMessage, FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertEquals("", $actual);

    // Create a user account.
    $sender = User::create(['name' => $this->randomMachineName()]);
    $sender->save();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPageEmail::createFromMessage($coreMessage, FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $expect = $sender->toUrl('canonical', ['absolute' => TRUE])->toString();
    $this->assertEquals($expect, $actual);
  }

  /**
   * Verify that message property is accessible via twig.
   */
  public function testContactPageMessageTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_test_theme')->save();

    // Create a contact form.
    // Note: Appropriate template is selected using theme suggestion via the
    // contact form id.
    $contactForm = ContactForm::create([
      'id' => 'contact_mesg_message_test',
      'label' => $this->randomMachineName(),
    ]);
    $contactForm->save();

    $messageLines = [
      $this->randomString(32),
      $this->randomString(28),
      $this->randomString(8),
    ];
    $contactMessage = $message = Message::create([
      'contact_form' => 'contact_mesg_message_test',
      'subject' => $this->randomMachineName(),
      'message' => implode("\n\n", $messageLines),
      'mail' => $this->randomMachineName() . '@example.com',
      'name' => $this->randomString(),
    ]);
    $message->save();

    $sender = User::getAnonymousUser();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_form'] = $contactForm;
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPageEmail::createFromMessage($coreMessage, FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());

    // Rendered markup is hard to predict. Thus, just assert that all the lines
    // are in the message.
    foreach ($messageLines as $line) {
      $this->assertStringContainsString(Html::escape($line), $actual);
    }
  }

}
