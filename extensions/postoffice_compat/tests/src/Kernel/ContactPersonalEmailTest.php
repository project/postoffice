<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\contact\Entity\Message;
use Drupal\postoffice_compat\Email\ContactPersonalEmail;
use Drupal\user\Entity\User;

/**
 * Tests for personal contact email.
 *
 * @group postoffice_compat
 */
class ContactPersonalEmailTest extends CompatTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['contact', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['contact', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('contact_message');
    $this->installEntitySchema('user');
  }

  /**
   * Verify that message properties and urls are accessible via twig.
   */
  public function testContactPersonalMessageTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_test_theme')->save();

    // Create a user account.
    $recipient = User::create(['name' => $this->randomMachineName()]);
    $recipient->save();

    $messageLines = [
      $this->randomString(32),
      $this->randomString(28),
      $this->randomString(8),
    ];
    $contactMessage = Message::create([
      'contact_form' => 'personal',
      'subject' => $this->randomMachineName(),
      'message' => implode("\n\n", $messageLines),
      'mail' => $this->randomMachineName() . '@example.com',
      'name' => $this->randomString(),
      'recipient' => $recipient,
    ]);
    $contactMessage->save();

    // Create a user account.
    $sender = User::create(['name' => $this->randomMachineName()]);
    $sender->save();

    $coreMessage = $this->createCoreMessage('contact', 'page_autoreply');
    $coreMessage['params']['contact_message'] = $contactMessage;
    $coreMessage['params']['sender'] = $sender;
    $email = ContactPersonalEmail::createFromMessage($coreMessage, FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());

    $recipientName = Html::escape($recipient->getDisplayName());
    $recipientEditUrl = $recipient->toUrl(
      'edit-form',
      ['absolute' => TRUE]
    )->toString();
    $expectedHead = <<<EOT
<p>{$recipientName} <a href="{$recipientEditUrl}">(Profile)</a></p>
EOT;

    $this->assertStringStartsWith($expectedHead, $actual);

    // Rendered markup is hard to predict. Thus, just assert that all the lines
    // are in the message.
    foreach ($messageLines as $line) {
      $this->assertStringContainsString(Html::escape($line), $actual);
    }
  }

}
