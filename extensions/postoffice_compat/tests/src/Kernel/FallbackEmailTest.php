<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Core\Url;
use Drupal\postoffice_compat\Email\FallbackEmail;

/**
 * Tests for fallback email.
 *
 * @group postoffice_compat
 */
class FallbackEmailTest extends CompatTestBase {

  /**
   * Verify that site properties are accessible via twig.
   */
  public function testFallbackSiteTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_compat_test_theme')->save();

    $coreMessage = $this->createCoreMessage('system', 'site_url_test');
    $email = FallbackEmail::createFromMessage($coreMessage);
    /** @var \Drupal\postoffice_compat\Email\FallbackEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $expected = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $this->assertEquals($expected, $actual);
  }

  /**
   * Tests sending a mail using a From address with a comma in it.
   */
  public function testMailWithCommaInFromAddress() {
    // Setup custom theme.
    $this->container->get('theme_installer')->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_compat_test_theme')->save();

    $coreMessage = $this->createCoreMessage('system', 'site_url_test');
    $coreMessage['headers']['From'] = '"Foo, Bar, and Baz" <from@example.org>';
    $email = FallbackEmail::createFromMessage($coreMessage);

    $addresses = $email->getFrom();
    $this->assertCount(1, $addresses);
    $this->assertEquals('Foo, Bar, and Baz', $addresses[0]->getName());
    $this->assertEquals('from@example.org', $addresses[0]->getAddress());
  }

}
