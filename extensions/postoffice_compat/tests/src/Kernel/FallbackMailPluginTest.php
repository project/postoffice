<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Markup;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests for fallback mail plugin.
 *
 * @group postoffice_compat
 */
class FallbackMailPluginTest extends CompatTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'postoffice_compat_fallback_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.mail')
      ->set('interface.postoffice_compat_fallback_test', 'postoffice_fallback_mail')
      ->save();

    // Setup custom theme.
    $this->container->get('theme_installer')->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_compat_test_theme')->save();
  }

  /**
   * Verify that the password reset mail is delivered using the UserMail plugin.
   */
  public function testCompatUserMailPlugin() {
    /** @var \Drupal\Core\Mail\MailManagerInterface $mailManager */
    $mailManager = $this->container->get('plugin.manager.mail');
    $to = $this->randomMachineName() . '@example.com';

    $plainLine = $this->randomString();
    $markupLine = '<p style="color: red">' . $this->randomMachineName() . '</p>';
    $params = [
      'subject' => $this->randomMachineName(),
      'body' => [
        $plainLine,
        Markup::create($markupLine),
      ],
    ];

    $recordedEmails = $this->callAndRecordEmails(function () use ($mailManager, $to, $params) {
      $mailManager->mail('postoffice_compat_fallback_test', 'markup_mix_test', $to, 'en', $params);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $expected = implode("\n\n\n", [
      '<p>' . Html::escape($plainLine) . '</p>',
      $markupLine,
    ]);
    $this->assertEquals($expected, $actual);
  }

}
