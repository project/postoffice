<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Core\Url;
use Drupal\postoffice_compat\Email\UserEmail;
use Drupal\user\Entity\User;

/**
 * Tests for user email.
 *
 * @group postoffice_compat
 */
class UserEmailTest extends CompatTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');
  }

  /**
   * Verify that account properties and urls are accessible via twig.
   */
  public function testUserAccountTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_compat_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_test_theme')->save();

    // Create a user account.
    $userAccount = User::create(['name' => $this->randomMachineName()]);
    $userAccount->save();

    // Tests email.accountCancelUrl.
    $coreMessage = $this->createCoreMessage('user', 'account_cancel_url_test');
    $coreMessage['params']['account'] = $userAccount;
    $email = UserEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $front = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $fragment = $front . 'user/' . $userAccount->id() . '/cancel/confirm/';
    $this->assertStringContainsString($fragment, $actual);

    // Tests email.accountCanonicalUrl.
    $coreMessage = $this->createCoreMessage('user', 'account_canonical_url_test');
    $coreMessage['params']['account'] = $userAccount;
    $email = UserEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $front = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $urlPrefix = $front . 'user';
    $this->assertStringContainsString($urlPrefix, $actual);

    // Tests email.account.displayName.
    $coreMessage = $this->createCoreMessage('user', 'account_display_name_test');
    $coreMessage['params']['account'] = $userAccount;
    $email = UserEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $expected = $userAccount->getDisplayName();
    $this->assertEquals($expected, $actual);

    // Tests email.accountEditUrl.
    $coreMessage = $this->createCoreMessage('user', 'account_edit_url_test');
    $coreMessage['params']['account'] = $userAccount;
    $email = UserEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $front = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $urlPrefix = $front . 'user/' . $userAccount->id() . '/edit';
    $this->assertStringContainsString($urlPrefix, $actual);

    // Tests email.account.email.
    $coreMessage = $this->createCoreMessage('user', 'account_email_test');
    $coreMessage['params']['account'] = $userAccount;
    $email = UserEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $expected = $userAccount->getEmail();
    $this->assertEquals($expected, $actual);

    // Tests email.account.accountName.
    $coreMessage = $this->createCoreMessage('user', 'account_name_test');
    $coreMessage['params']['account'] = $userAccount;
    $email = UserEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $expected = $userAccount->getAccountName();
    $this->assertEquals($expected, $actual);

    // Tests email.accountOneTimeLoginUrl.
    $coreMessage = $this->createCoreMessage('user', 'account_one_time_login_url_test');
    $coreMessage['params']['account'] = $userAccount;
    $email = UserEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $front = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $fragment = $front . 'user/reset/' . $userAccount->id();
    $this->assertStringContainsString($fragment, $actual);
  }

}
