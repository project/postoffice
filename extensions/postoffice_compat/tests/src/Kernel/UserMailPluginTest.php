<?php

namespace Drupal\Tests\postoffice_compat\Kernel;

use Drupal\Core\Url;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests for user mail plugin.
 *
 * @group postoffice_compat
 */
class UserMailPluginTest extends CompatTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');

    $this->config('system.mail')
      ->set('interface.user', 'postoffice_user_mail')
      ->save();
  }

  /**
   * Verify that the password reset mail is delivered using the UserMail plugin.
   */
  public function testCompatUserMailPlugin() {
    // Create a user account.
    $userAccount = $this->createUser([], NULL, FALSE, [
      'mail' => $this->randomMachineName() . '@example.com',
    ]);

    $recordedEmails = $this->callAndRecordEmails(function () use ($userAccount) {
      _user_mail_notify('password_reset', $userAccount);
    });

    $this->assertCount(1, $recordedEmails);
    /** @var \Drupal\postoffice_compat\Email\UserEmail $email */
    $email = end($recordedEmails);
    $this->assertEquals($userAccount->getEmail(), $email->getTo()[0]->toString());
    $actual = $email->getHtmlBody();
    $fragment = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString() . 'user/reset/' . $userAccount->id();
    $this->assertStringContainsString($fragment, $actual);
  }

}
