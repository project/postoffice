<?php

namespace Drupal\postoffice_compat_theme\Mail;

use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * Decorator for core MailManager which switches to the configured mail theme.
 */
class ThemeMailManager implements MailManagerInterface, PluginManagerInterface, CachedDiscoveryInterface, CacheableDependencyInterface {

  /**
   * Constructs core MailManager decorator which switches to the mail theme.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface|\Drupal\Component\Plugin\PluginManagerInterface|\Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface|\Drupal\Core\Cache\CacheableDependencyInterface $mailManager
   *   The mail manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme manager service.
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $themeInitialization
   *   The theme initialization service.
   */
  public function __construct(
    protected MailManagerInterface|PluginManagerInterface|CachedDiscoveryInterface|CacheableDependencyInterface $mailManager,
    protected ConfigFactoryInterface $configFactory,
    protected ThemeManagerInterface $themeManager,
    protected ThemeInitializationInterface $themeInitialization,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function mail($module, $key, $to, $langcode, $params = [], $reply = NULL, $send = TRUE) {
    $originalTheme = $this->themeManager->getActiveTheme()->getName();
    $mailTheme = $this->getMailTheme();

    try {
      if ($originalTheme !== $mailTheme) {
        $this->themeManager->setActiveTheme($this->themeInitialization->initTheme($mailTheme));
      }
      $message = $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
    }
    finally {
      $activeTheme = $this->themeManager->getActiveTheme()->getName();
      if ($activeTheme !== $originalTheme) {
        $this->themeManager->setActiveTheme($this->themeInitialization->initTheme($originalTheme));
      }
    }

    return $message;
  }

  /**
   * Determine the mail theme.
   */
  protected function getMailTheme() {
    $result = $this->configFactory->get('system.theme')->get('default');

    $config = $this->configFactory->get('postoffice.site');
    if (!$config->get('theme_use_default')) {
      $result = $config->get('theme_name');
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDefinition($plugin_id) {
    return $this->mailManager->hasDefinition($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition($plugin_id, $exception_on_invalid = TRUE) {
    return $this->mailManager->getDefinition($plugin_id, $exception_on_invalid);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    return $this->mailManager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return $this->mailManager->createInstance($plugin_id, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    return $this->mailManager->getInstance($options);
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    return $this->mailManager->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function useCaches($use_caches = FALSE) {
    return $this->mailManager->useCaches($use_caches);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->mailManager->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->mailManager->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->mailManager->getCacheMaxAge();
  }

}
