<?php

namespace Drupal\Tests\postoffice_twig\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;

/**
 * Tests whether postoffice theme is picked up for mails sent via legacy hook.
 *
 * @group postoffice_compat_theme
 */
class CompatThemeTest extends PostofficeTestBase {

  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'postoffice_compat_theme',
    'postoffice_compat_theme_hook_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['stark', 'postoffice_compat_theme_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_compat_theme_test_theme')->save();
  }

  /**
   * Test that the default theme is used even if another theme is active.
   */
  public function testDefaultTheme() {
    /** @var \Drupal\Core\Theme\ThemeManagerInterface $themeManager */
    $themeManager = $this->container->get('theme.manager');
    /** @var \Drupal\Core\Theme\ThemeInitializationInterface $themeInitialization */
    $themeInitialization = $this->container->get('theme.initialization');
    /** @var \Drupal\Core\Mail\MailManagerInterface */
    $mailManager = $this->container->get('plugin.manager.mail');

    // Assert that active theme is the default theme.
    $this->assertEquals(
      'postoffice_compat_theme_test_theme',
      $themeManager->getActiveTheme()->getName()
    );

    // Switch the current theme temporary to stark.
    $themeManager->setActiveTheme($themeInitialization->initTheme('stark'));

    // Assert that active theme is stark.
    $this->assertEquals('stark', $themeManager->getActiveTheme()->getName());

    // Send mail using legacy mail manager.
    $to = $this->randomMachineName() . '@example.com';
    $mailManager->mail('postoffice_compat_theme_hook_test', 'ignored', $to, 'en');

    // Assert that active theme when sending mail with mail manager was the
    // default theme.
    $this->assertMail('subject', 'Current active theme');
    $this->assertMail('body', "The currently active theme is: postoffice_compat_theme_test_theme\n");

    // Assert that active theme is still the temporary stark.
    $this->assertEquals('stark', $themeManager->getActiveTheme()->getName());
  }

}
