<?php

namespace Drupal\Tests\postoffice_file\Kernel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\file\Functional\FileFieldCreationTrait;
use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\file\Entity\File;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for file adjustments subscriber handling native email.
 *
 * @group postoffice_file
 */
class FileAdjustmentNativeTest extends PostofficeTestBase {

  use TestFileCreationTrait;
  use FileFieldCreationTrait;
  use ThirdPartySettingsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'text',
    'entity_test',
    'file',
    'postoffice_file_compat_test',
    'postoffice_file',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['file', 'field']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('entity_test');

    $this->config('system.mail')
      ->set('interface.postoffice_file_compat_test', 'postoffice_fallback_mail')
      ->save();

    $this->createFileField('attachments', 'entity_test', 'entity_test');
  }

  /**
   * Verify that file attachments propagate through to email and get hidden.
   */
  public function testAttachAndRemove() {
    /** @var stdClass */
    $fixture = current($this->getTestFiles('text'));
    $file = File::create((array) $fixture + ['status' => 1]);
    $file->save();

    $this->setThirdPartySettings('attach_remove', 'attachments', 'entity_test', 'entity_test', 'default');

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'attachments' => [
        $file,
      ],
    ]);

    $email = new PostofficeTestEmail('en', $this->viewEntity($entity, 'default'));

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'text/plain disposition: attachment filename: ' . $file->getFilename();
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);

    $this->assertStringNotContainsString($file->getFilename(), $recordedEmails[0]->getHtmlBody());
  }

  /**
   * Verify that files are attached to an email and URLs are transformed.
   */
  public function testAttachAndTransform() {
    /** @var stdClass */
    $fixture = current($this->getTestFiles('text'));
    $file = File::create((array) $fixture + ['status' => 1]);
    $file->save();

    $this->setThirdPartySettings('attach_transform_urls', 'attachments', 'entity_test', 'entity_test', 'default');

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'attachments' => [
        $file,
      ],
    ]);

    $email = new PostofficeTestEmail('en', $this->viewEntity($entity, 'default'));

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'text/plain disposition: attachment filename: ' . $file->getFilename();
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);

    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $generator */
    $generator = $this->container->get('file_url_generator');
    $fileUrl = $generator->generate($file->getFileUri())->setAbsolute()->toString();
    $this->assertStringContainsString($fileUrl, $recordedEmails[0]->getHtmlBody());
  }

  /**
   * Verify that files are attached to an email and field is displayed.
   */
  public function testAttachAndDisplay() {
    /** @var stdClass */
    $fixture = current($this->getTestFiles('text'));
    $file = File::create((array) $fixture + ['status' => 1]);
    $file->save();

    $this->setThirdPartySettings('attach_display', 'attachments', 'entity_test', 'entity_test', 'default');

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'attachments' => [
        $file,
      ],
    ]);

    $email = new PostofficeTestEmail('en', $this->viewEntity($entity, 'default'));

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'text/plain disposition: attachment filename: ' . $file->getFilename();
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);

    $this->assertStringContainsString($file->getFilename(), $recordedEmails[0]->getHtmlBody());
  }

  /**
   * Verify that file url is transformed in an email message.
   */
  public function testTransformUrls() {
    /** @var stdClass */
    $fixture = current($this->getTestFiles('text'));
    $file = File::create((array) $fixture + ['status' => 1]);
    $file->save();

    $this->setThirdPartySettings('transform_urls', 'attachments', 'entity_test', 'entity_test', 'default');

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'attachments' => [
        $file,
      ],
    ]);

    $email = new PostofficeTestEmail('en', $this->viewEntity($entity, 'default'));

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(0, $recordedAttachments);

    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $generator */
    $generator = $this->container->get('file_url_generator');
    $fileUrl = $generator->generate($file->getFileUri())->setAbsolute()->toString();
    $this->assertStringContainsString($fileUrl, $recordedEmails[0]->getHtmlBody());
  }

  /**
   * Return entity render array.
   */
  protected function viewEntity(ContentEntityInterface $entity, string $viewMode = 'default'): array {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entityTypeManager = $this->container->get('entity_type.manager');
    $viewBuilder = $entityTypeManager->getViewBuilder($entity->getEntityTypeId());
    return $viewBuilder->view($entity, $viewMode);
  }

}
