<?php

namespace Drupal\Tests\postoffice_file\Kernel;

use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;
use Drupal\file\Entity\File;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for file attachment twig extension.
 *
 * @group postoffice_file
 */
class FileAttachmentTwigExtensionTest extends PostofficeTestBase {

  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'user',
    'postoffice_file',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['user', 'file']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');

    // Setup custom theme.
    $this->container->get('theme_installer')->install(['postoffice_file_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_file_test_theme')->save();
  }

  /**
   * Verify that file entities can be attached using a twig function.
   */
  public function testThemedEmailAttachEntityFunction() {
    /** @var stdClass */
    $fixture = current($this->getTestFiles('text'));
    $file = File::create((array) $fixture + ['status' => 1]);
    $file->save();

    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_file_test_attach_entity_function',
      '#file' => $file,
    ]);

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'text/plain disposition: attachment filename: ' . $file->getFilename();
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that file can be attached using a twig function.
   */
  public function testThemedEmailAttachUriFunction() {
    /** @var stdClass */
    $fixture = current($this->getTestFiles('text'));

    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_file_test_attach_uri_function',
      '#uri' => $fixture->uri,
    ]);

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'text/plain disposition: attachment filename: ' . $fixture->filename;
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);
  }

}
