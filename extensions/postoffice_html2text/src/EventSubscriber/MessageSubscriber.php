<?php

namespace Drupal\postoffice_html2text\EventSubscriber;

use Soundasleep\Html2Text;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;

/**
 * Message subscriber which is responsible for inlining CSS.
 *
 * Runs with priority -100 in order (after Symfony MessageListener which is
 * responsible for body rendering).
 */
class MessageSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];

    if (class_exists('\Soundasleep\Html2Text')) {
      $events[MessageEvent::class] = ['onMessage', -100];

    }

    return $events;
  }

  /**
   * Generate a text fallback if possible.
   */
  public function onMessage(MessageEvent $event): void {
    $message = $event->getMessage();
    if ($message instanceof Email && $message->getHtmlBody() && !$message->getTextBody()) {
      $message->text(Html2Text::convert($message->getHtmlBody()));
    }
  }

}
