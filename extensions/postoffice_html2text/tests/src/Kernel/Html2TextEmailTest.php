<?php

namespace Drupal\Tests\postoffice_html2text\Kernel;

use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for html2text subscriber.
 *
 * @group postoffice_html2text
 */
class Html2TextEmailTest extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['postoffice_html2text'];

  /**
   * Verify that emails are converted to text.
   */
  public function testHtml2Text() {
    $markup = <<<'EOT'
<h1>Hello, World!</h1>

<p>This is some email content.
Even though it has whitespace and newlines, the email converter
will handle it correctly.

<p>Even mismatched tags.</p>

<div>A div</div>
<div>Another div</div>
<div>A div<div>within a div</div></div>

<p>Another line<br />Yet another line</p>

<a href="http://foo.com">A link</a>
EOT;

    $expected = <<<'EOT'
Hello, World!

This is some email content. Even though it has whitespace and newlines, the email converter will handle it correctly.

Even mismatched tags.

A div
Another div
A div
within a div

Another line
Yet another line

[A link](http://foo.com)
EOT;

    $email = new PostofficeTestEmail('en', [
      '#markup' => $markup,
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getTextBody());
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that existing text is not overwritten.
   */
  public function testNoHtml2PreexistingText() {
    $markup = '<h1>Hello, HTML World!</h1>';
    $text = 'Hello, Text World!';

    $email = new PostofficeTestEmail('en', [
      '#markup' => $markup,
    ]);
    $email->text($text);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getTextBody());
    $this->assertEquals($text, $actual);
  }

  /**
   * Verify that an email with an empty body is not modified.
   */
  public function testHtml2TextEmptyEmail() {
    $email = new PostofficeTestEmail('en', ['#markup' => '']);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getHtmlBody();

    $expected = '';
    $this->assertEquals($expected, $actual);
  }

}
