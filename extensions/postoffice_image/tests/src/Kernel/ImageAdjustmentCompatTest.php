<?php

namespace Drupal\Tests\postoffice_image\Kernel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\postoffice_compat\Kernel\CompatTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\file\Entity\File;

/**
 * Tests for image adjustments subscriber handling compat email.
 *
 * @group postoffice_image
 */
class ImageAdjustmentCompatTest extends CompatTestBase {

  use TestFileCreationTrait;
  use ImageFieldCreationTrait;
  use ThirdPartySettingsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'text',
    'entity_test',
    'file',
    'image',
    'postoffice_compat',
    'postoffice_image_compat_test',
    'postoffice_image',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['file', 'field', 'image']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('entity_test');

    $this->config('system.mail')
      ->set('interface.postoffice_image_compat_test', 'postoffice_fallback_mail')
      ->save();

    $this->createImageField('image', 'entity_test', 'entity_test');
  }

  /**
   * Verify that image is embedded into an email message.
   */
  public function testEmbed() {
    /** @var \Drupal\Core\Mail\MailManagerInterface $mailManager */
    $mailManager = $this->container->get('plugin.manager.mail');
    $to = $this->randomMachineName() . '@example.com';

    /** @var stdClass */
    $fixture = current(array_filter(
      $this->getTestFiles('image'),
      fn($file) => str_ends_with($file->filename, '.jpg')
    ));
    $file = File::create((array) $fixture + ['status' => 1]);
    $file->save();

    $this->setThirdPartySettings('embed', 'image', 'entity_test', 'entity_test', 'default');

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'image' => [
        $file,
      ],
    ]);

    $params = [
      'subject' => $this->randomMachineName(),
      'body' => $this->viewEntity($entity, 'default'),
    ];

    /** @var \Drupal\postoffice_compat\Email\FallbackEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($mailManager, $to, $params) {
      ['result' => $result] = $mailManager->mail('postoffice_image_compat_test', 'test', $to, 'en', $params);
      $this->assertTrue($result);
    });

    $this->assertCount(1, $recordedEmails);
    $pattern = '|<img [^>]*src="cid:([^"]+)"[^>]*>|';
    $this->assertMatchesRegularExpression($pattern, $recordedEmails[0]->getHtmlBody());
    preg_match($pattern, $recordedEmails[0]->getHtmlBody(), $matches);
    $cid = $matches[1];
    $this->assertNotEmpty($cid);

    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'image/jpeg disposition: inline filename: ' . $cid;
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);

    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $generator */
    $generator = $this->container->get('file_url_generator');
    $imageUrl = $generator->generateAbsoluteString($file->getFileUri());
    $this->assertStringNotContainsString($imageUrl, $recordedEmails[0]->getHtmlBody());
  }

  /**
   * Verify that image url is transformed in an email message.
   */
  public function testTransformUrls() {
    /** @var \Drupal\Core\Mail\MailManagerInterface $mailManager */
    $mailManager = $this->container->get('plugin.manager.mail');
    $to = $this->randomMachineName() . '@example.com';

    /** @var stdClass */
    $fixture = current(array_filter(
      $this->getTestFiles('image'),
      fn($file) => str_ends_with($file->filename, '.jpg')
    ));
    $file = File::create((array) $fixture + ['status' => 1]);
    $file->save();

    $this->setThirdPartySettings('transform_urls', 'image', 'entity_test', 'entity_test', 'default');

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'image' => [
        $file,
      ],
    ]);

    $params = [
      'subject' => $this->randomMachineName(),
      'body' => $this->viewEntity($entity, 'default'),
    ];

    /** @var \Drupal\postoffice_compat\Email\FallbackEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($mailManager, $to, $params) {
      ['result' => $result] = $mailManager->mail('postoffice_image_compat_test', 'test', $to, 'en', $params);
      $this->assertTrue($result);
    });

    $this->assertCount(1, $recordedEmails);
    $pattern = '|<img [^>]*src="cid:([^"]+)"[^>]*>|';
    $this->assertDoesNotMatchRegularExpression($pattern, $recordedEmails[0]->getHtmlBody());

    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(0, $recordedAttachments);

    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $generator */
    $generator = $this->container->get('file_url_generator');
    $imageUrl = $generator->generateAbsoluteString($file->getFileUri());
    $this->assertStringContainsString($imageUrl, $recordedEmails[0]->getHtmlBody());
  }

  /**
   * Return entity render array.
   */
  protected function viewEntity(ContentEntityInterface $entity, string $viewMode = 'default'): array {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entityTypeManager = $this->container->get('entity_type.manager');
    $viewBuilder = $entityTypeManager->getViewBuilder($entity->getEntityTypeId());
    return $viewBuilder->view($entity, $viewMode);
  }

}
