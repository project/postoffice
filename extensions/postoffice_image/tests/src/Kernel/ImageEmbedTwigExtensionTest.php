<?php

namespace Drupal\Tests\postoffice_image\Kernel;

use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;
use Drupal\image\Entity\ImageStyle;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for image embed twig extension.
 *
 * @group postoffice_image
 */
class ImageEmbedTwigExtensionTest extends PostofficeTestBase {

  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'image',
    'user',
    'postoffice_image',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['user', 'file', 'image']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');

    // Setup custom theme.
    $this->container->get('theme_installer')->install(['postoffice_image_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_image_test_theme')->save();

    // Ensure the system is configured with some jpeg image quality.
    $this->config('system.image.gd')
      ->set('jpeg_quality', 75)
      ->save();
  }

  /**
   * Verify that images can be embedded using a twig filter.
   */
  public function testThemedEmailImagePathFilter() {
    /** @var stdClass */
    $fixture = current(array_filter(
      $this->getTestFiles('image'),
      fn($file) => str_ends_with($file->filename, '.jpg')
    ));

    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_image_test_embed_image_path_filter',
      '#path' => $fixture->uri,
    ]);

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $pattern = '|<img src="cid:([^"]+)">|';
    $this->assertMatchesRegularExpression($pattern, $recordedEmails[0]->getHtmlBody());
    preg_match($pattern, $recordedEmails[0]->getHtmlBody(), $matches);
    $cid = $matches[1];
    $this->assertNotEmpty($cid);

    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'image/jpeg disposition: inline filename: ' . $cid;
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that images can be embedded using a twig function.
   */
  public function testThemedEmailImagePathFunction() {
    /** @var stdClass */
    $fixture = current(array_filter(
      $this->getTestFiles('image'),
      fn($file) => str_ends_with($file->filename, '.jpg')
    ));

    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_image_test_embed_image_path_function',
      '#path' => $fixture->uri,
    ]);

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $pattern = '|<img src="cid:([^"]+)">|';
    $this->assertMatchesRegularExpression($pattern, $recordedEmails[0]->getHtmlBody());
    preg_match($pattern, $recordedEmails[0]->getHtmlBody(), $matches);
    $cid = $matches[1];
    $this->assertNotEmpty($cid);

    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'image/jpeg disposition: inline filename: ' . $cid;
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that image style can be embedded using a twig filter.
   */
  public function testThemedEmailImageStyleFilter() {
    /** @var stdClass */
    $fixture = current(array_filter(
      $this->getTestFiles('image'),
      fn($file) => str_ends_with($file->filename, '.jpg')
    ));

    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator */
    $fileUrlGenerator = $this->container->get('file_url_generator');
    $style = ImageStyle::create(['name' => 'test', 'label' => 'Test']);
    $style->save();

    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_image_test_embed_image_style_filter',
      '#uri' => $fixture->uri,
      '#style' => $style->id(),
    ]);

    // Ensure that the derivative image does not exist before the email is
    // rendered and sent.
    $this->assertFalse(file_exists($style->buildUri($fixture->uri)));

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    // Ensure that the derivative image exists after the email is rendered
    // and sent.
    $this->assertTrue(file_exists($style->buildUri($fixture->uri)));

    $this->assertCount(1, $recordedEmails);
    $pattern = '|<img src="cid:([^"]+)">|';
    $this->assertMatchesRegularExpression($pattern, $recordedEmails[0]->getHtmlBody());
    preg_match($pattern, $recordedEmails[0]->getHtmlBody(), $matches);
    $cid = $matches[1];
    $this->assertNotEmpty($cid);

    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'image/jpeg disposition: inline filename: ' . $cid;
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that image style can be embedded using a twig function.
   */
  public function testThemedEmailImageStyleFunction() {
    /** @var stdClass */
    $fixture = current(array_filter(
      $this->getTestFiles('image'),
      fn($file) => str_ends_with($file->filename, '.jpg')
    ));

    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator */
    $fileUrlGenerator = $this->container->get('file_url_generator');
    $style = ImageStyle::create(['name' => 'test', 'label' => 'Test']);
    $style->save();

    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_image_test_embed_image_style_function',
      '#uri' => $fixture->uri,
      '#style' => $style->id(),
    ]);

    // Ensure that the derivative image does not exist before the email is
    // rendered and sent.
    $this->assertFalse(file_exists($style->buildUri($fixture->uri)));

    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    // Ensure that the derivative image exists after the email is rendered and
    // sent.
    $this->assertTrue(file_exists($style->buildUri($fixture->uri)));

    $this->assertCount(1, $recordedEmails);
    $pattern = '|<img src="cid:([^"]+)">|';
    $this->assertMatchesRegularExpression($pattern, $recordedEmails[0]->getHtmlBody());
    preg_match($pattern, $recordedEmails[0]->getHtmlBody(), $matches);
    $cid = $matches[1];
    $this->assertNotEmpty($cid);

    $recordedAttachments = $recordedEmails[0]->getAttachments();
    $this->assertCount(1, $recordedAttachments);

    $expected = 'image/jpeg disposition: inline filename: ' . $cid;
    $actual = $recordedAttachments[0]->asDebugString();
    $this->assertEquals($expected, $actual);
  }

}
