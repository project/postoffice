<?php

namespace Drupal\Tests\postoffice_image\Kernel;

/**
 * Provides a helper method for third party settings on a field formatter.
 */
trait ThirdPartySettingsTrait {

  /**
   * Sets postoffice image third party setting on given field.
   *
   * @param string $adjustment
   *   Which adjustment to apply when processing the image for emails.
   * @param string $fieldName
   *   The name of the field to create.
   * @param string $entityTypeId
   *   The entity type id on which the field is to be created.
   * @param string $bundle
   *   The entity bundle on which the field is to be created.
   * @param string $viewMode
   *   The view mode on which the field is to be configured.
   */
  public function setThirdPartySettings(
    string $adjustment,
    string $fieldName = 'image',
    string $entityTypeId = 'entity_test',
    string $bundle = 'entity_test',
    string $viewMode = 'default',
  ): int {

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $displayRepository */
    $displayRepository = $this->container->get('entity_display.repository');

    $display = $displayRepository->getViewDisplay($entityTypeId, $bundle, $viewMode);
    $component = $display->getComponent($fieldName);
    $component['third_party_settings']['postoffice_image'] = [
      'adjustment' => $adjustment,
    ];
    $display->setComponent($fieldName, $component);
    return $display->save();
  }

}
