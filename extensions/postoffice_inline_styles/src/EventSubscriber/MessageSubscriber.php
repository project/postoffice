<?php

namespace Drupal\postoffice_inline_styles\EventSubscriber;

use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Message;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

/**
 * Message subscriber which is responsible for inlining CSS.
 *
 * Runs with priority -150 in order (after Symfony MessageListener which is
 * responsible for body rendering).
 */
class MessageSubscriber implements EventSubscriberInterface {

  /**
   * The asset resolver.
   */
  protected AssetResolverInterface $assetResolver;

  /**
   * Constructs a new inline styles message subscriber.
   */
  public function __construct(AssetResolverInterface $assetResolver) {
    $this->assetResolver = $assetResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];

    if (class_exists('\TijsVerkoyen\CssToInlineStyles\CssToInlineStyles')) {
      $events[MessageEvent::class] = ['onMessage', -150];
    }

    return $events;
  }

  /**
   * Inline CSS if possible.
   */
  public function onMessage(MessageEvent $event): void {
    $message = $event->getMessage();
    if ($message instanceof Email && $message->getHtmlBody()) {
      $inliner = new CssToInlineStyles();
      $css = $this->cssFromAttachments($message);
      $message->html($inliner->convert($message->getHtmlBody(), $css));
    }
  }

  /**
   * Returns CSS from attached libraries if message has attached assets.
   */
  protected function cssFromAttachments(Message $message): string {
    $chunks = [];

    if ($message instanceof TemplateAttachmentsInterface) {
      $assets = $message->getTemplateAttachments();
      $paths = $this->assetResolver->getCssAssets($assets, FALSE);

      $chunks = array_map(
        fn($asset) => @file_get_contents($asset['data']) ?: "",
        $paths,
      );
    }

    return implode("\n", $chunks);
  }

}
