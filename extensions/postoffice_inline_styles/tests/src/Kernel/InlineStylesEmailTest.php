<?php

namespace Drupal\Tests\postoffice_inline_styles\Kernel;

use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for inline styles subscriber.
 *
 * @group postoffice_inline_styles
 */
class InlineStylesEmailTest extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['postoffice_inline_styles'];

  /**
   * Verify that styles get inlined into markup.
   */
  public function testInlineStylesEmail() {
    $email = new PostofficeTestEmail('en', [
      '#markup' => '<h1>Hello, green world!</h1>',
      '#attached' => [
        'library' => [
          'postoffice_test/green',
        ],
      ],
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getHtmlBody();

    $expected = <<<'EOT'
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><h1 style="color: green;">Hello, green world!</h1></body></html>
EOT;
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that an email with an empty body is not modified.
   */
  public function testInlineStylesEmptyEmail() {
    $email = new PostofficeTestEmail('en', ['#markup' => '']);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getHtmlBody();

    $expected = '';
    $this->assertEquals($expected, $actual);
  }

}
