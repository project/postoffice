<?php

namespace Drupal\postoffice_skel\EventSubscriber;

use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Render\RendererInterface;
use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;

/**
 * Message subscriber which wraps a message into a HTML document skeleton.
 *
 * Runs with priority -125 (after Symfony MessageListener which is responsible
 * for body rendering, after html2text, before inline styles).
 */
class MessageSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a postoffice skel message subscriber.
   *
   * @param \Drupal\Core\Render\RendererInterface $coreRenderer
   *   The renderer.
   */
  public function __construct(protected RendererInterface $coreRenderer) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[MessageEvent::class] = ['onMessage', -125];
    return $events;
  }

  /**
   * Wrap a message into a HTML document skeleton.
   */
  public function onMessage(MessageEvent $event): void {
    $message = $event->getMessage();
    if ($message instanceof Email && $message->getHtmlBody()) {
      $build = ['#theme' => 'postoffice_skel', '#email' => $message];
      if ($message instanceof TemplateAttachmentsInterface) {
        $templateAttachments = $message->getTemplateAttachments();
        $build['#attached']['library'] = $templateAttachments->getLibraries();
        $build['#attached']['drupalSettings'] = $templateAttachments->getSettings();
      }
      $message->html((string) $this->coreRenderer->render($build));
      if ($message instanceof TemplateAttachmentsInterface) {
        $templateAttachments = AttachedAssets::createFromRenderArray($build);
        $message->setTemplateAttachments($templateAttachments);
      }

    }
  }

}
