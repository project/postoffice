<?php

namespace Drupal\Tests\postoffice_html2text\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for HTML skeleton subscriber.
 *
 * @group postoffice_skel
 */
class SkelTest extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['postoffice_skel'];

  /**
   * Verify that emails are converted to text.
   */
  public function testSkel() {
    $markup = $this->randomMachineName();

    $templateAttachments = [
      'library' => [
        'foo/bar',
      ],
      'drupalSettings' => [
        'foo' => 'bar',
      ],
    ];
    $email = new PostofficeTestEmail('en', [
      '#markup' => $markup,
      '#attached' => $templateAttachments,
    ]);
    /** @var \Drupal\postoffice_test\Email\PostofficeTestEmail[] $recordedEmails */
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    [$result] = $recordedEmails;
    $this->assertStringContainsString(Html::escape($markup), $result->getHtmlBody());
    $this->assertStringContainsString('lang="en"', $result->getHtmlBody());
    $this->assertStringContainsString('dir="ltr"', $result->getHtmlBody());
    $this->assertStringContainsString('<!DOCTYPE html>', $result->getHtmlBody());
    $this->assertEquals(
      $templateAttachments['library'],
      $result->getTemplateAttachments()->getLibraries()
    );
    $this->assertEquals(
      $templateAttachments['drupalSettings'],
      $result->getTemplateAttachments()->getSettings()
    );
  }

  /**
   * Verify that an email with an empty body is not modified.
   */
  public function testSkelEmptyEmail() {
    $email = new PostofficeTestEmail('en', ['#markup' => '']);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getHtmlBody();

    $expected = '';
    $this->assertEquals($expected, $actual);
  }

}
