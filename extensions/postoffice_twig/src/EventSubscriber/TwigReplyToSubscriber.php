<?php

namespace Drupal\postoffice_twig\EventSubscriber;

use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;

/**
 * Sets the reply-to name and/or address defined by a twig filter.
 *
 * Runs with priority -50 (after Symfony MessageListener which is responsible
 * for body rendering).
 */
class TwigReplyToSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[MessageEvent::class] = ['onMessage', -50];
    return $events;
  }

  /**
   * Populates the reply-to address defined postoffice_subject twig filter.
   */
  public function onMessage(MessageEvent $event): void {
    $message = $event->getMessage();
    if ($message instanceof Email && $message instanceof TemplateAttachmentsInterface) {
      $settings = $message->getTemplateAttachments()->getSettings();
      if (isset($settings['postofficeReplyToAddress'])) {
        $message->replyTo($settings['postofficeReplyToAddress']);
      }
    }
  }

}
