<?php

namespace Drupal\postoffice_twig\EventSubscriber;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;

/**
 * Sets the subject defined by the postoffice_subject twig filter.
 *
 * Runs with priority -50 (after Symfony MessageListener which is responsible
 * for body rendering).
 */
class TwigSubjectSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[MessageEvent::class] = ['onMessage', -50];
    return $events;
  }

  /**
   * Populates the subject defined postoffice_subject twig filter.
   */
  public function onMessage(MessageEvent $event): void {
    $message = $event->getMessage();
    if ($message instanceof Email && $message instanceof TemplateAttachmentsInterface) {
      $settings = $message->getTemplateAttachments()->getSettings();
      if (isset($settings['postofficeSubject'])) {
        $subject = PlainTextOutput::renderFromHtml($settings['postofficeSubject']);
        $message->subject($subject);
      }
    }
  }

}
