<?php

namespace Drupal\postoffice_twig\EventSubscriber;

use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;

/**
 * Sets the text body defined by the postoffice_text_body twig filter.
 *
 * Runs with priority -50 (after Symfony MessageListener which is responsible
 * for body rendering, before html2text which automatically generates a text
 * body from the html part).
 */
class TwigTextBodySubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[MessageEvent::class] = ['onMessage', -50];
    return $events;
  }

  /**
   * Populates the text body defined postoffice_text_body twig filter.
   */
  public function onMessage(MessageEvent $event): void {
    $message = $event->getMessage();
    if ($message instanceof Email && $message instanceof TemplateAttachmentsInterface) {
      $settings = $message->getTemplateAttachments()->getSettings();
      if (isset($settings['postofficeTextBody'])) {
        $message->text($settings['postofficeTextBody']);
      }
    }
  }

}
