<?php

namespace Drupal\postoffice_twig\Template;

use Drupal\Core\Render\RendererInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * A class providing postoffice Twig extensions.
 */
class TwigExtension extends AbstractExtension {

  /**
   * The renderer.
   */
  protected RendererInterface $renderer;

  /**
   * Constructs postoffice twig extension.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('postoffice_from_address', $this->addFromAddress(...)),
      new TwigFunction('postoffice_reply_to_address', $this->addReplyToAddress(...)),
      new TwigFunction('postoffice_subject', $this->addSubject(...)),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('postoffice_from_address', $this->addFromAddress(...)),
      new TwigFilter('postoffice_reply_to_address', $this->addReplyToAddress(...)),
      new TwigFilter('postoffice_subject', $this->addSubject(...)),
      new TwigFilter('postoffice_text_body', $this->addTextBody(...)),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'postoffice';
  }

  /**
   * Sets the from address on the email template.
   *
   * Allows Twig templates to set the from address using a function.
   * @code
   * {% postoffice_from_address('Example <mail@example.com>') %}
   * @endcode
   * Or a filter
   * @code
   * {% 'no-reply@example.com'|postoffice_from_address %}
   * @endcode
   *
   * @param string $address
   *   Set the from address.
   */
  public function addFromAddress(string $address) {
    assert(is_string($address), 'Argument must be a string.');
    $templateAttached['#attached']['drupalSettings']['postofficeFromAddress'] = $address;
    $this->renderer->render($templateAttached);
  }

  /**
   * Sets the reply-to address on the email template.
   *
   * Allows Twig templates to set the reply-to address using a function.
   * @code
   * {% postoffice_reply_to_address('Example <mail@example.com>') %}
   * @endcode
   * Or a filter
   * @code
   * {% 'no-reply@example.com'|postoffice_reply_to_address %}
   * @endcode
   *
   * @param string $address
   *   Set the reply-to address.
   */
  public function addReplyToAddress(string $address) {
    assert(is_string($address), 'Argument must be a string.');
    $templateAttached['#attached']['drupalSettings']['postofficeReplyToAddress'] = $address;
    $this->renderer->render($templateAttached);
  }

  /**
   * Sets a subject on the email template.
   *
   * Allows Twig templates to set a subject using a function.
   * @code
   * {% postoffice_subject('This subject from @site'|t({'@site': email.getSiteName()})) %}
   * @endcode
   * Or a filter
   * @code
   * {% 'This subject from @site'|t({'@site': email.getSiteName()})|postoffice_subject %}
   * @endcode
   *
   * @param string $subject
   *   Set the subject.
   */
  public function addSubject(string $subject) {
    assert(is_string($subject), 'Argument must be a string.');
    $templateAttached['#attached']['drupalSettings']['postofficeSubject'] = $subject;
    $this->renderer->render($templateAttached);
  }

  /**
   * Attaches a text body to the email template.
   *
   * Allows Twig templates to attach a text body part using.
   * @code
   * {% apply postoffice_text_body %}
   * Text body here, for {{ things }} and {{ stuff }}.
   * {% endapply %}
   * @endcode
   *
   * @param string $body
   *   Set the text body.
   */
  public function addTextBody(string $body) {
    assert(is_string($body), 'Argument must be a string.');
    $templateAttached['#attached']['drupalSettings']['postofficeTextBody'] = $body;
    $this->renderer->render($templateAttached);
  }

}
