<?php

namespace Drupal\Tests\postoffice_twig\Kernel;

use Drupal\Tests\postoffice\Kernel\PostofficeTestBase;
use Drupal\postoffice_test\Email\PostofficeTestEmail;
use Symfony\Component\Mime\Address;

/**
 * Tests for subject and text body twig extensions.
 *
 * @group postoffice_twig
 */
class TwigExtensionTest extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'postoffice_twig',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Setup custom theme.
    $this->container->get('theme_installer')
      ->install(['postoffice_twig_test_theme']);
    $this->config('system.theme')
      ->set('default', 'postoffice_twig_test_theme')->save();
  }

  /**
   * Verify that the from field can be set using a twig filter from a template.
   */
  public function testThemedEmailAddressFilter() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_twig_test_from_address_filter',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getFrom();
    $expected = [new Address("no-reply@example.com", "Example Inc.")];
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that the from field can be set using a twig function.
   */
  public function testThemedEmailAddressFunction() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_twig_test_from_address_function',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getFrom();
    $expected = [new Address("no-reply@example.com", "Example Inc.")];
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that the reply-to field can be set using a twig filter.
   */
  public function testThemedEmailReplyToFilter() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_twig_test_reply_to_filter',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getReplyTo();
    $expected = [new Address("no-reply@example.com", "Example Inc.")];
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that the reply-to field can be set using a twig function.
   */
  public function testThemedEmailReplyToFunction() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_twig_test_reply_to_function',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getReplyTo();
    $expected = [new Address("no-reply@example.com", "Example Inc.")];
    $this->assertEquals($expected, $actual);
  }

  /**
   * Verify that the subject can be set using a twig filter from a template.
   */
  public function testThemedEmailSubjectFilter() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_twig_test_subject_filter',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getSubject();
    $this->assertEquals('subject from twig filter', $actual);
  }

  /**
   * Verify that the subject can be set using a twig function from a template.
   */
  public function testThemedEmailSubjectFunction() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_twig_test_subject_function',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getSubject();
    $this->assertEquals('subject from twig function', $actual);
  }

  /**
   * Verify that the text body can be set using a twig filter from a template.
   */
  public function testThemedEmailTextBody() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_twig_test_text_body',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getTextBody());
    $this->assertEquals('text part by test module', $actual);
  }

}
