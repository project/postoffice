<?php

namespace Drupal\postoffice\Email;

/**
 * Interface for localized email messages.
 */
interface LocalizedEmailInterface {

  /**
   * Return the desired language.
   */
  public function getLangcode(): string;

}
