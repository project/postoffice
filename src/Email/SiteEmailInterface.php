<?php

namespace Drupal\postoffice\Email;

/**
 * Interface for site email.
 */
interface SiteEmailInterface {

  /**
   * Returns the site name.
   *
   * Accessible via email.siteName from twig templates.
   */
  public function getSiteName(): string;

  /**
   * Returns the site slogan.
   *
   * Accessible via email.siteSlogan from twig templates.
   */
  public function getSiteSlogan(): string;

  /**
   * Returns the site mail address.
   *
   * Accessible via email.siteMail from twig templates.
   */
  public function getSiteEmail(): string;

  /**
   * Returns the site URL.
   *
   * Accessible via email.siteUrl from twig templates.
   */
  public function getSiteUrl(): string;

  /**
   * Returns the URL of the site's front page without the protocol.
   *
   * Accessible via email.siteUrlBrief from twig templates.
   */
  public function getSiteUrlBrief(): string;

  /**
   * Returns the URL of the site's login page.
   *
   * Accessible via email.siteLoginUrl from twig templates.
   */
  public function getSiteLoginUrl(): string;

}
