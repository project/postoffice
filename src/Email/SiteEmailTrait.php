<?php

namespace Drupal\postoffice\Email;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Url;

/**
 * Implements site email interface.
 */
trait SiteEmailTrait {

  use UrlOptionsTrait;

  /**
   * Site configuration.
   */
  protected ?ImmutableConfig $siteConfig;

  /**
   * {@inheritdoc}
   */
  public function getSiteName(): string {
    return $this->getSiteConfig()->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteSlogan(): string {
    return $this->getSiteConfig()->get('slogan');
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteEmail(): string {
    return $this->getSiteConfig()->get('mail');
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteUrl(): string {
    return Url::fromRoute('<front>', [], $this->getUrlOptions())->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteUrlBrief(): string {
    return preg_replace(['!^https?://!', '!/$!'], '', $this->getSiteUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteLoginUrl(): string {
    return Url::fromRoute('user.page', [], $this->getUrlOptions())->toString();
  }

  /**
   * Returns the site configuration.
   */
  protected function getSiteConfig(): ImmutableConfig {
    return $this->siteConfig ?? \Drupal::config('system.site');
  }

}
