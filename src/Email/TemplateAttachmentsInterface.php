<?php

namespace Drupal\postoffice\Email;

use Drupal\Core\Asset\AttachedAssets;

/**
 * Interface for messages using #attached libraries or settings.
 */
interface TemplateAttachmentsInterface {

  /**
   * Sets assets and settings attached when mail was rendered.
   */
  public function setTemplateAttachments(AttachedAssets $attachedAssets): void;

  /**
   * Returns assets and settings attached when mail was rendered.
   */
  public function getTemplateAttachments(): AttachedAssets;

}
