<?php

namespace Drupal\postoffice\Email;

use Drupal\Core\Asset\AttachedAssets;

/**
 * Trait which implements the TemplateAttachmentsInterface.
 */
trait TemplateAttachmentsTrait {

  /**
   * Attached assets.
   */
  protected ?AttachedAssets $attachedAssets;

  /**
   * Sets assets and settings attached when mail was rendered.
   */
  public function setTemplateAttachments(AttachedAssets $attachedAssets): void {
    $this->attachedAssets = $attachedAssets;
  }

  /**
   * Returns assets and settings attached when mail was rendered.
   */
  public function getTemplateAttachments(): AttachedAssets {
    return $this->attachedAssets ?? new AttachedAssets();
  }

}
