<?php

namespace Drupal\postoffice\Email;

/**
 * Interface for email messages with a HTML body produced using a render array.
 */
interface ThemedEmailInterface {

  /**
   * Mail constructor.
   *
   * @return array
   *   The render array.
   */
  public function buildThemedEmail(): ?array;

}
