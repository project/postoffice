<?php

namespace Drupal\postoffice\Email;

use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Trait which provides url options used when urls need to be generated.
 */
trait UrlOptionsTrait {

  /**
   * Language manager.
   */
  protected ?LanguageManagerInterface $languageManager;

  /**
   * Returns default url options.
   */
  protected function getUrlOptions(): array {
    $options = ['absolute' => TRUE];
    if ($this instanceof LocalizedEmailInterface) {
      $options['language'] = $this->getLanguageManager()->getLanguage($this->getLangcode());
    }
    return $options;
  }

  /**
   * Returns the language manager.
   */
  protected function getLanguageManager(): LanguageManagerInterface {
    return $this->languageManager ?? \Drupal::languageManager();
  }

}
