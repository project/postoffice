<?php

namespace Drupal\postoffice\Form;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\Exception\ExceptionInterface as SymfonyMailerException;
use Symfony\Component\Mailer\Transport;

/**
 * Configure postoffice settings for this site.
 */
class SiteSettingsForm extends ConfigFormBase {

  /**
   * Theme handler.
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('theme_handler'));
  }

  /**
   * Constructs a new site settings form.
   */
  public function __construct(ThemeHandlerInterface $themeHandler) {
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'postoffice_site_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['postoffice.site'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $siteConfig = $this->config('postoffice.site');

    $form['transport'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Transport'),
    ];
    $form['transport']['dsn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail transport DSN'),
      '#description' => $this->t('The mail transport DSN. See <a href=":uri">Symfony Mailer documentation</a> for more details.', [
        ':uri' => 'https://symfony.com/doc/current/mailer.html',
      ]),
      '#default_value' => $siteConfig->get('dsn'),
    ];

    $defaultName = $this->themeHandler->getDefault();
    $form['theme'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Theme'),
    ];
    $form['theme']['theme_use_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use default theme (@name)', [
        '@name' => $this->themeHandler->getName($defaultName),
      ]),
      '#description' => $this->t('Use the default frontend theme to render look up templates. Uncheck to choose from a list of enabled themes.'),
      '#default_value' => $siteConfig->get('theme_use_default'),
    ];

    $options = array_map(
      fn (Extension $extension) => $extension->getName(),
      array_filter(
        $this->themeHandler->listInfo(),
        fn (Extension $extension) => $extension->status
      )
    );
    $form['theme']['theme_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Custom theme'),
      '#options' => $options,
      '#description' => $this->t('Choose a custom theme if the <em>Use default theme</em> checkbox is unchecked.'),
      '#default_value' => $siteConfig->get('theme_name'),
      '#states' => [
        'visible' => [
          ':input[name="theme_use_default"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      Transport::fromDsn($form_state->getValue('dsn'));
    }
    catch (SymfonyMailerException $e) {
      $form_state->setError($form['dsn'], $this->t('Invalid DSN: @error', [
        '@error' => $e->getMessage(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('postoffice.site')
      ->set('dsn', $form_state->getValue('dsn'))
      ->set('theme_use_default', (bool) $form_state->getValue('theme_use_default'))
      ->set('theme_name', $form_state->getValue('theme_name'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
