<?php

namespace Drupal\postoffice\Language;

/**
 * Defines the interface for language switcher service.
 */
interface LanguageSwitcherInterface {

  /**
   * Changes the active language for translations.
   *
   * @param string $langcode
   *   The langcode.
   */
  public function switchTo(string $langcode): void;

}
