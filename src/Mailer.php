<?php

namespace Drupal\postoffice;

use Drupal\Core\Config\ConfigFactoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Mail class.
 */
class Mailer implements MailerInterface {

  /**
   * The event dispatcher.
   */
  protected TransportInterface $transport;

  /**
   * Create a new mailer.
   *
   * @param \Psr\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(EventDispatcherInterface $eventDispatcher, ConfigFactoryInterface $configFactory) {
    $dsn = $configFactory->get('postoffice.site')->get('dsn');
    $this->transport = Transport::fromDsn($dsn, $eventDispatcher);
  }

  /**
   * {@inheritdoc}
   */
  public function send(RawMessage $message, ?Envelope $envelope = NULL): void {
    $this->transport->send($message, $envelope);
  }

}
