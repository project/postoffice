<?php

namespace Drupal\postoffice\MailerMiddleware;

use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Implements a mailer middleware which creates a new render context.
 */
class CleanRenderContext implements MailerInterface {

  /**
   * The decorated mailer.
   */
  protected MailerInterface $mailer;

  /**
   * The drupal core renderer.
   */
  protected RendererInterface $renderer;

  /**
   * Constructs new anonymous account mailer middleware.
   */
  public function __construct(MailerInterface $mailer, Renderer $renderer) {
    $this->mailer = $mailer;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function send(RawMessage $message, ?Envelope $envelope = NULL): void {
    $this->renderer->executeInRenderContext(
      new RenderContext(),
      fn() => $this->mailer->send($message, $envelope)
    );
  }

}
