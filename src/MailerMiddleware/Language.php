<?php

namespace Drupal\postoffice\MailerMiddleware;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\postoffice\Email\LocalizedEmailInterface;
use Drupal\postoffice\Language\LanguageSwitcherInterface;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Implements the language mailer middleware.
 */
class Language implements MailerInterface {

  /**
   * The decorated mailer.
   */
  protected MailerInterface $mailer;

  /**
   * The language manager.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The language switcher.
   */
  protected LanguageSwitcherInterface $languageSwitcher;

  /**
   * Constructs a new language mailer middleware.
   */
  public function __construct(MailerInterface $mailer, LanguageManagerInterface $languageManager, LanguageSwitcherInterface $languageSwitcher) {
    $this->mailer = $mailer;
    $this->languageManager = $languageManager;
    $this->languageSwitcher = $languageSwitcher;
  }

  /**
   * {@inheritdoc}
   */
  public function send(RawMessage $message, ?Envelope $envelope = NULL): void {
    $originalLangcode = $this->languageManager->getCurrentLanguage()->getId();
    $mailLangcode = $this->getLangcode($message);

    try {
      if ($originalLangcode !== $mailLangcode) {
        $this->languageSwitcher->switchTo($mailLangcode);
      }
      $this->mailer->send($message, $envelope);
    }
    finally {
      $activeLanguage = $this->languageManager->getCurrentLanguage()->getId();
      if ($activeLanguage !== $originalLangcode) {
        $this->languageSwitcher->switchTo($originalLangcode);
      }
    }
  }

  /**
   * Determine the language for this message.
   */
  public function getLangcode(RawMessage $message): string {
    return ($message instanceof LocalizedEmailInterface) ?
      $message->getLangcode() :
      $this->languageManager->getDefaultLanguage()->getId();
  }

}
