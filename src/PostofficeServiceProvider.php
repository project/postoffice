<?php

namespace Drupal\postoffice;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\postoffice\DependencyInjection\Compiler\StackedMailerPass;

/**
 * Custom mailer dependency injection container.
 */
class PostofficeServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // Add a compiler pass for adding body renderer middlewares.
    $container->addCompilerPass(new StackedMailerPass());
  }

}
