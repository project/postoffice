<?php

namespace Drupal\postoffice;

use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Outermost layer of a mailer stack.
 */
class StackedMailer implements MailerInterface {

  /**
   * The decorated mailer.
   */
  protected MailerInterface $mailer;

  /**
   * Constructs a stacked mailer.
   */
  public function __construct(MailerInterface $mailer) {
    $this->mailer = $mailer;
  }

  /**
   * {@inheritdoc}
   */
  public function send(RawMessage $message, ?Envelope $envelope = NULL): void {
    $this->mailer->send($message, $envelope);
  }

}
