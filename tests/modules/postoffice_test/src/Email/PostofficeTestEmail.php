<?php

namespace Drupal\postoffice_test\Email;

use Drupal\postoffice\Email\LocalizedEmailInterface;
use Drupal\postoffice\Email\SiteEmailInterface;
use Drupal\postoffice\Email\SiteEmailTrait;
use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Drupal\postoffice\Email\TemplateAttachmentsTrait;
use Drupal\postoffice\Email\ThemedEmailInterface;
use Symfony\Component\Mime\Email;

/**
 * Email test double.
 */
class PostofficeTestEmail extends Email implements LocalizedEmailInterface, SiteEmailInterface, ThemedEmailInterface, TemplateAttachmentsInterface {

  use TemplateAttachmentsTrait;
  use SiteEmailTrait;

  /**
   * The language code for this message.
   */
  protected string $langcode;

  /**
   * The render array for this message.
   */
  protected array $build;

  /**
   * Creates new email test double.
   */
  public function __construct(string $langcode, array $build) {
    parent::__construct();
    $this->langcode = $langcode;
    $this->build = $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(): string {
    return $this->langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function buildThemedEmail(): ?array {
    return $this->build;
  }

  /**
   * Injects the #email variable into the render array.
   */
  public function injectEmailBuildVar(): static {
    $this->build['#email'] = $this;
    return $this;
  }

}
