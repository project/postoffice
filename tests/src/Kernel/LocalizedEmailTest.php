<?php

// phpcs:disable Drupal.Semantics.FunctionT.NotLiteralString

namespace Drupal\Tests\postoffice\Kernel;

use Drupal\locale\StringInterface;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for localized email.
 *
 * @group postoffice
 */
class LocalizedEmailTest extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language',
    'locale',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['language']);
    $this->installSchema('locale', [
      'locales_location',
      'locales_source',
      'locales_target',
    ]);

    // Ensure we are building a new Language object for each test.
    $this->container->get('language_manager')->reset();

    // Activate Spanish language, so there are two languages activated.
    $language = $this->container->get('entity_type.manager')->getStorage('configurable_language')->create([
      'id' => 'es',
    ]);
    $language->save();
  }

  /**
   * Verify that emails are localized when rendered.
   */
  public function testLocalizedEmail() {
    $source = $this->buildSourceString()->save();
    $this->assertNotEmpty($source->lid);
    $translations = $this->createAllTranslations($source);

    $email = new PostofficeTestEmail('es', [
      '#markup' => t($source->source, [], ['context' => $source->context]),
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getHtmlBody();
    $this->assertEquals($translations['es']->translation, $actual);
  }

  /**
   * Verify that middleware uses default locale for untranslated strings.
   */
  public function testLocalizedEmailWithUntranslatedString() {
    $source = $this->buildSourceString()->save();
    $this->assertNotEmpty($source->lid);

    $email = new PostofficeTestEmail('es', [
      '#markup' => t($source->source, [], ['context' => $source->context]),
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getHtmlBody();
    $this->assertEquals($source->source, $actual);
  }

  /**
   * Verify that middleware uses default locale if langcode is invalid.
   */
  public function testLocalizedEmailWithInvalidLangcode() {
    $source = $this->buildSourceString()->save();
    $this->assertNotEmpty($source->lid);
    $translations = $this->createAllTranslations($source);

    $email = new PostofficeTestEmail('xx-invalid', [
      '#markup' => t($source->source, [], ['context' => $source->context]),
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = $recordedEmails[0]->getHtmlBody();
    $this->assertEquals($source->source, $actual);
  }

  /**
   * Creates random source string object.
   *
   * @param array $values
   *   The values array.
   *
   * @return \Drupal\locale\StringInterface
   *   A locale string.
   */
  protected function buildSourceString(array $values = []) {
    return $this->container->get('locale.storage')->createString($values += [
      'source' => $this->randomMachineName(100),
      'context' => $this->randomMachineName(20),
    ]);
  }

  /**
   * Creates translations for source string and all languages.
   *
   * @param \Drupal\locale\StringInterface $source
   *   The source string.
   * @param array $values
   *   The values array.
   *
   * @return array
   *   Translation list.
   */
  protected function createAllTranslations(StringInterface $source, array $values = []) {
    $list = [];
    /** @var \Drupal\Core\Language\LanguageManagerInterface $language_manager */
    $language_manager = $this->container->get('language_manager');
    foreach ($language_manager->getLanguages() as $language) {
      $list[$language->getId()] = $this->createTranslation($source, $language->getId(), $values);
    }
    return $list;
  }

  /**
   * Creates single translation for source string.
   *
   * @param \Drupal\locale\StringInterface $source
   *   The source string.
   * @param string $langcode
   *   The language code.
   * @param array $values
   *   The values array.
   *
   * @return \Drupal\locale\StringInterface
   *   The translated string object.
   */
  protected function createTranslation(StringInterface $source, $langcode, array $values = []) {
    return $this->container->get('locale.storage')->createTranslation($values + [
      'lid' => $source->lid,
      'language' => $langcode,
      'translation' => $this->randomMachineName(100),
    ])->save();
  }

}
