<?php

namespace Drupal\Tests\postoffice\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\Mailer\Event\MessageEvent;

/**
 * Base class for postoffice kernel tests.
 *
 * @group postoffice
 */
abstract class PostofficeTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'postoffice',
    'postoffice_test',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['postoffice']);

    // Ensure no mails are sent.
    $this->config('postoffice.site')
      ->set('dsn', 'null://null')
      ->save();

    // Ensure the system is configured with a site name and email address.
    $this->config('system.site')
      ->set('mail', 'simpletest@example.com')
      ->set('name', $this->randomMachineName(12))
      ->save();
  }

  /**
   * Runs the given callable and returns emails captured during its execution.
   */
  protected function callAndRecordEmails(callable $func): array {
    $result = [];
    $captureEmail = function (MessageEvent $event) use (&$result) {
      $result[] = $event->getMessage();
    };
    $this->container->get('event_dispatcher')->addListener(MessageEvent::class, $captureEmail, -1000);
    try {
      $func();
    }
    finally {
      $this->container->get('event_dispatcher')->removeListener(MessageEvent::class, $captureEmail);
    }
    return $result;
  }

}
