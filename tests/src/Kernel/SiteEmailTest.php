<?php

namespace Drupal\Tests\postoffice\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for site email trait.
 *
 * @group postoffice
 */
class SiteEmailTest extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');
  }

  /**
   * Verify that the site properties can be set using a twig variable.
   */
  public function testSiteEmailTwigProperties() {
    // Tests email.siteLoginUrl.
    $email = (new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email_site_login_url',
    ]))->injectEmailBuildVar();
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $expected = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString() . 'user';
    $this->assertEquals($expected, $actual);

    // Tests email.siteEmail.
    $email = (new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email_site_email',
    ]))->injectEmailBuildVar();
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $expected = $this->config('system.site')->get('mail');
    $this->assertEquals($expected, $actual);

    // Tests email.siteName.
    $email = (new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email_site_name',
    ]))->injectEmailBuildVar();
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $expected = $this->config('system.site')->get('name');
    $this->assertEquals($expected, $actual);

    // Tests email.siteSlogan.
    $slogan = $this->randomString();
    $this->config('system.site')->set('slogan', $slogan)->save();
    $email = (new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email_site_slogan',
    ]))->injectEmailBuildVar();
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $this->assertEquals(Html::escape($slogan), $actual);

    // Tests email.siteUrlBrief.
    $email = (new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email_site_url_brief',
    ]))->injectEmailBuildVar();
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $expected = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $expected = preg_replace(['!^https?://!', '!/$!'], '', $expected);
    $this->assertEquals($expected, $actual);

    // Tests email.siteUrl.
    $email = (new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email_site_url',
    ]))->injectEmailBuildVar();
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertNotEmpty($actual);
    $expected = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $this->assertEquals($expected, $actual);

  }

}
