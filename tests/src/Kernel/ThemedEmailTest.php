<?php

namespace Drupal\Tests\postoffice\Kernel;

use Drupal\postoffice_test\Email\PostofficeTestEmail;

/**
 * Tests for themed email.
 *
 * @group postoffice
 */
class ThemedEmailTest extends PostofficeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'postoffice',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('theme_installer')->install(['postoffice_test_theme']);
    $this->config('system.theme')->set('default', 'stark')->save();
  }

  /**
   * Verify that emails use module templates when rendered.
   */
  public function testThemedEmailDefaultTemplate() {
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertEquals('rendered by test module', $actual);
  }

  /**
   * Verify that emails use templates from theme if configured.
   */
  public function testThemedEmailThemeTemplate() {
    $this->config('postoffice.site')
      ->set('theme_use_default', FALSE)
      ->set('theme_name', 'postoffice_test_theme')
      ->save();
    $email = new PostofficeTestEmail('en', [
      '#theme' => 'postoffice_test_email',
    ]);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertEquals('rendered by test theme', $actual);
  }

}
