<?php

namespace Drupal\Tests\postoffice\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\postoffice\DependencyInjection\Compiler\StackedMailerPass;
use Drupal\postoffice\StackedMailer;
use Symfony\Component\DependencyInjection\Definition;

/**
 * @coversDefaultClass \Drupal\postoffice\DependencyInjection\Compiler\StackedMailerPass
 * @group postoffice
 */
class StackedMailerPassTest extends UnitTestCase {

  /**
   * The stacked mailer pass.
   */
  protected StackedMailerPass $stackedMailerPass;

  /**
   * The container builder.
   */
  protected ContainerBuilder $containerBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->stackedMailerPass = new StackedMailerPass();
    $this->containerBuilder = new ContainerBuilder();
  }

  /**
   * @covers ::process
   */
  public function testProcessWithStackedMailer() {
    $stacked_kernel = new Definition(StackedMailer::class);
    $stacked_kernel->setPublic(TRUE);
    $this->containerBuilder->setDefinition('postoffice.mailer', $stacked_kernel);
    $this->containerBuilder->setDefinition('postoffice.mailer.basic', $this->createMiddlewareServiceDefinition(FALSE, 0));

    $this->containerBuilder->setDefinition('postoffice.mailer.three', $this->createMiddlewareServiceDefinition());
    $this->containerBuilder->setDefinition('postoffice.mailer.one', $this->createMiddlewareServiceDefinition(TRUE, 10));
    $this->containerBuilder->setDefinition('postoffice.mailer.two', $this->createMiddlewareServiceDefinition(TRUE, 5));

    $this->stackedMailerPass->process($this->containerBuilder);

    $stacked_mailer_args = $this->containerBuilder->getDefinition('postoffice.mailer')->getArguments();

    // Check the stacked mailer args.
    $this->assertSame('postoffice.mailer.one', (string) $stacked_mailer_args[0]);

    // Check the modified definitions.
    $definition = $this->containerBuilder->getDefinition('postoffice.mailer.one');
    $args = $definition->getArguments();
    $this->assertSame('postoffice.mailer.two', (string) $args[0]);
    $this->assertSame('test', $args[1]);

    $definition = $this->containerBuilder->getDefinition('postoffice.mailer.two');
    $args = $definition->getArguments();
    $this->assertSame('postoffice.mailer.three', (string) $args[0]);
    $this->assertSame('test', $args[1]);

    $definition = $this->containerBuilder->getDefinition('postoffice.mailer.three');
    $args = $definition->getArguments();
    $this->assertSame('postoffice.mailer.basic', (string) $args[0]);
    $this->assertSame('test', $args[1]);
  }

  /**
   * @covers ::process
   */
  public function testProcessWithAnyMailer() {
    $kernel = new Definition('Symfony\Component\Mailer\MailerInterface');
    $kernel->setPublic(TRUE);
    $this->containerBuilder->setDefinition('postoffice.mailer', $kernel);
    $this->stackedMailerPass->process($this->containerBuilder);

    $unprocessed_mailer = $this->containerBuilder->getDefinition('postoffice.mailer');

    $this->assertSame($kernel, $unprocessed_mailer);
    $this->assertSame($kernel->getArguments(), $unprocessed_mailer->getArguments());
  }

  /**
   * Creates a middleware definition.
   *
   * @param bool $tag
   *   Whether or not to set the postoffice.mailer_middleware tag.
   * @param int $priority
   *   The priority to be used for the tag.
   */
  protected function createMiddlewareServiceDefinition($tag = TRUE, $priority = 0): Definition {
    $definition = new Definition('Symfony\Component\Mailer\MailerInterface', ['test']);
    $definition->setPublic(TRUE);

    if ($tag) {
      $definition->addTag('postoffice.mailer_middleware', ['priority' => $priority]);
    }

    return $definition;
  }

}
