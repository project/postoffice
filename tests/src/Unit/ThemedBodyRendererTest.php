<?php

namespace Drupal\Tests\postoffice\Unit;

use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Render\RendererInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\postoffice\BodyRenderer\ThemedBodyRenderer;
use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Drupal\postoffice\Email\ThemedEmailInterface;
use Prophecy\Argument;
use Symfony\Component\Mime\Email;

/**
 * @coversDefaultClass \Drupal\postoffice\BodyRenderer\ThemedBodyRenderer
 * @group postoffice
 */
class ThemedBodyRendererTest extends UnitTestCase {

  /**
   * Verify that symfony email remains untouched.
   */
  public function testRenderSymfonyEmail(): void {
    $email = $this->prophesize(Email::class);
    $coreRenderer = $this->prophesize(RendererInterface::class);

    $coreRenderer->render(Argument::any(), Argument::any())->shouldNotBeCalled();
    $email->html(Argument::any())->shouldNotBeCalled();

    $renderer = new ThemedBodyRenderer($coreRenderer->reveal());
    $renderer->render($email->reveal());
  }

  /**
   * Verify that themed email is built.
   */
  public function testRenderThemedEmail(): void {
    $email = $this->prophesize(Email::class)
      ->willImplement(ThemedEmailInterface::class)
      ->willImplement(TemplateAttachmentsInterface::class);
    $coreRenderer = $this->prophesize(RendererInterface::class);

    $renderArray = [
      '#theme' => 'hello_world',
      '#attached' => [
        'library' => [
          'postoffice/email.test',
        ],
        'drupalSettings' => [
          'postofficeTest' => 'Hello!',
        ],
      ],
    ];
    $templateAttachments = (new AttachedAssets())
      ->setLibraries(['postoffice/email.test'])
      ->setSettings(['postofficeTest' => 'Hello!']);
    $markup = '<p>hello world!</p>';

    $email->buildThemedEmail()->willReturn($renderArray);
    $coreRenderer->render($renderArray, TRUE)->willReturn($markup);
    $email->html($markup)->willReturn($email);
    $email->setTemplateAttachments($templateAttachments)->shouldBeCalled();

    $renderer = new ThemedBodyRenderer($coreRenderer->reveal());
    $renderer->render($email->reveal());
  }

}
